module bitbucket.org/finn0/gopkgtest

require (
	cloud.google.com/go v0.77.0
	github.com/certifi/gocertifi v0.0.0-20180905225744-ee1a9a0726d2 // indirect
	github.com/client9/reopen v1.0.0
	github.com/getsentry/raven-go v0.2.0
	github.com/getsentry/sentry-go v0.9.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/lightstep/lightstep-tracer-common/golang/gogo v0.0.0-20210210170715-a8dfcb80d3a7 // indirect
	github.com/lightstep/lightstep-tracer-go v0.24.0
	github.com/oklog/ulid/v2 v2.0.2
	github.com/opentracing/opentracing-go v1.2.0
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/prometheus/client_golang v1.9.0
	github.com/prometheus/client_model v0.2.0
	github.com/sebest/xff v0.0.0-20160910043805-6c115e0ffa35
	github.com/sirupsen/logrus v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/tinylib/msgp v1.0.2 // indirect
	github.com/uber-go/atomic v1.3.2 // indirect
	github.com/uber/jaeger-client-go v2.15.0+incompatible
	github.com/uber/jaeger-lib v1.5.0 // indirect
	google.golang.org/api v0.40.0
	google.golang.org/grpc v1.35.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.7.0
)

go 1.15